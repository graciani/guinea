const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const htmlmin = require('gulp-htmlmin');
 
/*exports.default = () => (
    gulp.src('imagenes/*')
        .pipe(imagemin())
        .pipe(gulp.dest('imagenesMin/'))
);*/


// gulp.task('minify', function() {
//     return gulp.src('*.html')
//     .pipe(htmlmin({ collapseWhitespace: true }))
//     .pipe(gulp.dest('htmlMin/'));
//     });

//   gulp.task('default', gulp.series(['minify']));

gulp.task('minify_images', function() {
     return gulp.src('imagenes/*')
        .pipe(imagemin())
        .pipe(gulp.dest('imagenesMin/'))
});

 gulp.task('minify_html', function() {
     return gulp.src('*.html')
     .pipe(htmlmin({ collapseWhitespace: true }))
     .pipe(gulp.dest('htmlMin/'));
     });

 gulp.task('default', gulp.series(['minify_images', 'minify_html']));